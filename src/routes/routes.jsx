import { Suspense } from "react";
import { lazy } from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import SkeletonLayout from "../components/skeleton/skeletonLayout";

const Home = lazy( () => import('../views/home'))
const Bitcoin = lazy( () => import('../views/bitcoin'))
const Profile = lazy( () => import('../views/profile'))
const Weather = lazy( () => import('../views/weather'))
const Notes = lazy( () => import('../views/notes'))
const Map = lazy( () => import('../views/map'))
const Covid = lazy( () => import('../views/covid'))
const Contacts = lazy( () => import('../views/contacts'))
const Post = lazy( () => import('../views/post'))
const Pomodoro = lazy( () => import('../views/pomodoro'))
const SleepTracker = lazy( () => import('../views/sleepTracker'))

const Routes = () => {

  // TODO: Skeleton to loading
  return (
    <Suspense fallback={<SkeletonLayout />}>
      <Router>
        <nav className="sidebar">
          <h1>Dashboard {/* <span>Demo</span> */}</h1>
          
          <ul className="sidebar__list">
              <li className="sidebar__list-item">
                <Link to="/">Home</Link>
              </li>
              <li className="sidebar__list-item">
                <Link to="/profile">Profile</Link>
              </li>
              <li className="sidebar__list-item">
                <Link to="/weather">Weather</Link>
              </li>
              <li className="sidebar__list-item">
                <Link to="/covid">Covid</Link>
              </li>
              <li className="sidebar__list-item">
                <Link to="/notes">Notes</Link>
              </li>
              <li className="sidebar__list-item">
                <Link to="/contacts">Contacs</Link>
              </li>
              <li className="sidebar__list-item">
                <Link to="/map">Map</Link>
              </li>
              <li className="sidebar__list-item">
                <Link to="/post">Post</Link>
              </li>
              <li className="sidebar__list-item">
                <Link to="/cryptos">Cryptos</Link>
              </li>
              <li className="sidebar__list-item">
                <Link to="/pomodoro">Pomodoro</Link>
              </li>
              <li className="sidebar__list-item">
                <Link to="/sleep-tracker">Sleep Daily</Link>
              </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/sleep-tracker">
            <SleepTracker />
          </Route>
          <Route path="/pomodoro">
            <Pomodoro />
          </Route>
          <Route path="/cryptos">
            <Bitcoin />
          </Route>
          <Route path="/post">
            <Post />
          </Route>
          <Route path="/map">
            <Map />
          </Route>
          <Route path="/contacts">
            <Contacts />
          </Route>
          <Route path="/notes">
            <Notes /> 
          </Route>
          <Route path="/covid">
            <Covid/> 
          </Route>
          <Route path="/weather">
            <Weather />
          </Route>
          <Route path="/profile">
            <Profile />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </Suspense>
  );
};

export default Routes;