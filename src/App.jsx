import MainLayout from "./layout/mainLayout"
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';

function App() {
  // Create a client
  const queryClient = new QueryClient({
   defaultOptions: {
     queries: {
       retry: 0,
       refetchOnWindowFocus: false,
     },
   },
 })

  return (
    <QueryClientProvider client={queryClient}>
      <MainLayout>
      </MainLayout>
    </QueryClientProvider>
  )
}

export default App
