
import LayoutDashboard from '../layout/layoutDashboard';

const Home = () => {

  return (
    <LayoutDashboard className="home-page"  title={""}>
      <h1>HOME</h1>
      <h2><b><i>work in progress</i></b></h2>
      <h2>This Dashboard is made with...</h2>

      <h3><a href="https://jestjs.io/">Jest</a></h3>
      <h3><a href="https://vitejs.dev/">Vite</a></h3>
      <h3><a href="https://es.reactjs.org/">React</a></h3>
      <h3><a href="https://es.redux.js.org/">Redux</a></h3>
      <h3><a href="https://postcss.org/">Postcss</a></h3>
      <h3><a href="https://docs.cypress.io">Cypress</a></h3>
      <h3><a href="https://apexcharts.com/">Apexcharts</a></h3>
      <h3><a href="https://react-query.tanstack.com/">React Query</a></h3>
      <h3><a href="https://github.com/reduxjs/redux-thunk">Redux thunk</a></h3>
      <h3><a href="https://github.com/rt2zz/redux-persist">Redux persist</a></h3>

    </LayoutDashboard>
  );
};

export default Home; 

// TODO: https://www.codementor.io/projects/web/daily-sleep-tracker-web-app-byi4kpk5rt
// TODO: https://www.codementor.io/projects/mobile/invoicing-and-payment-reminder-mobile-app-atx32o85yj
