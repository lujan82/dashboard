export const NEW_NOTE = "NEW_NOTE"
export const REMOVE_NOTE = "REMOVE_NOTE"
export const UPDATE_NOTE = "UPDATE_NOTE"

export const NEW_CONTACT = "NEW_CONTACT"
export const REMOVE_CONTACT = "REMOVE_CONTACT"
export const UPDATE_CONTACT = "UPDATE_CONTACT"

export const UPDATE_POST = "UPDATE_POST"

export const IS_CURRENT_DAY = "IS_CURRENT_DAY"
