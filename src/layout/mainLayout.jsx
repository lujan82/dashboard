import React from 'react';
import Routes from '../routes/routes';

const MainLayout = ({children}) => {
  return (
    <main>
      <Routes />
      {children}
    </main>
  );
};

export default MainLayout;